

//----------------------------------------------------this is for mongoose Atlas online Database------------------------

require('dotenv').config();

const mongoose = require('mongoose');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
app.use(cors());
app.use(bodyParser.json());

mongoose.connect(process.env.MONGODB_URI);

app.use('/register', require('./components/signup'));
app.use('/login', require('./components/login'));
app.use('/fetch', require('./components/fetch'));
app.use('/delete', require('./components/delete'));
app.use('/update', require('./components/update'));

const PORT = process.env.PORT || 4000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});


//--------------------------------------------------------this is for local Database-------------------------------------------



// const client = require('mongodb').MongoClient;
// const mongoose = require('mongoose');
// const express  =  require('express');
// const bodyparser = require('body-parser');
// const app = express();
// const cors = require('cors')
// app.use(cors())

// app.use(bodyparser.json());
// app.use('/register',require('./components/signup'))
// app.use('/login',require('./components/login'))
// app.use('/fetch',require('./components/fetch'))             
// app.use('/delete',require('./components/delete'))
// app.use('/update',require('./components/update'))

// app.listen(4000,(req,res)=>{
//     console.log("server is ok")
// })
