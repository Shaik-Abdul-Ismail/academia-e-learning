// signup.js

//----------------------------------------------------this is for mongoose Atlas online Database------------------------


const express = require('express');
const router = express.Router();
const User = require('./userModel'); 

router.post('/', async (req, res) => {
  try {
    const userExists = await User.exists({ email: req.body.email });
    if (userExists) {
      console.log('User data already exists');
      return res.status(201).json(); 
    } else {
      const newUser = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: req.body.password,
      });
      await newUser.save();
      console.log('User data inserted');
      res.status(201).json({ message: 'User data inserted', userExists: false });
    }
  } catch (error) {
    console.error(error);
    res.status(500).send({ message: 'Internal Server Error' });
  }
});

module.exports = router;


//--------------------------------------------------------this is for local Database-------------------------------------------


// let mongodb = require('mongodb');
// let client = mongodb.MongoClient;
// let register = require('express').Router().post('/', async (req, res) => {
//   try {
   
//     const userExists = await checkIfUserExists(req.body.email);

//     if (userExists) {
//         console.log('User data already exists');
//       return res.status(201).json({ message: 'User data already exists', userExists: true });
     
//     } else {
      
//       client.connect('mongodb://127.0.0.1:27017/new', (err, db) => {
//         if (err) {
//           throw err;
//         } else {
//           db.collection('data').insertOne(
//             {
//               firstName: req.body.firstName,
//               lastName: req.body.lastName,
//               email: req.body.email,
//               password: req.body.password,
//             },
//             (err, result) => {
//               if (err) {
//                 throw err;
//               } else {
//                 console.log('User data inserted')
//                 res.status(201).json({ message: 'User data inserted', userExists: false });
//                 db.close();
//               }
//             }
//           );
//         }
//       });
//     }
//   } catch (error) {
//     console.error(error);
  
//   }
// });

// async function checkIfUserExists(email) {
//   return new Promise((resolve, reject) => {
//     client.connect('mongodb://127.0.0.1:27017/new', async (err, db) => {
//       if (err) {
//         reject(err);
//       } else {
//         const existingUser = await db.collection('data').findOne({ email: email });
//         db.close();
//         resolve(!!existingUser); // Resolve true if user exists, false otherwise
//       }
//     });
//   });
// }

// module.exports = register;