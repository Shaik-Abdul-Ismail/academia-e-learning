

//----------------------------------------------------this is for mongoose Atlas online Database------------------------


const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const User = require('./userModel'); 

router.delete('/:email', async (req, res) => {
  try {
    await User.deleteOne({ email: req.params.email });
    res.send({ message: 'Document deleted' });
  } catch (error) {
    console.error(error);
    res.status(500).send({ message: 'Internal Server Error' });
  }
});

module.exports = router;



//---------------------------------------------this is working-----------------------------



//  let mongodb = require("mongodb");
// let client = mongodb.MongoClient;
// let delete1 = require("express")
//   .Router()
//   .delete("/:email", (req, res) => {
//     client.connect("mongodb://127.0.0.1:27017/new", (err, db) => {
//       if (err) {
//         throw err;
//       } else {
//         db.collection("data").deleteOne({email:req.params.email}, (err, result) => {
//           if (err) {
//             throw err;
//           } else {
//             res.send(result);
            
//             db.close();
//           }
//         });
//       }
//     });
//   }); 
// module.exports = delete1;
