

//----------------------------------------------------this is for mongoose Atlas online Database------------------------


const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const User = mongoose.model('User'); 

router.get('/', async (req, res) => {
  try {
    const users = await User.find({});
    if (users.length > 0) {
      res.send(users);
    } else {
      res.send({ message: 'Record Not Found...' });
    }
  } catch (error) {
    console.error(error);
    res.status(500).send({ message: 'Internal Server Error' });
  }
});

module.exports = router;


//--------------------------------------------------------this is for local Database-------------------------------------------


// var express = require("express");
// var bodyParser = require("body-parser");
// let mongodb = require("mongodb");
// let client = mongodb.MongoClient;

// let fetch = express.Router().get("/", (req, res) => {
//   client.connect("mongodb://127.0.0.1:27017/new", (err, db) => {
//     if (err) {
//       throw err;
//     } else {
//       console.log("fetch called...");
//       db.collection("data")
//         .find({})
//         .toArray((err, array) => {
//           if (err) {
//             throw err;
//           } else {
//             if (array.length > 0) {
//               res.send(array);
//             } else {
//               res.send({ message: "Record Not Found..." });
//             }
//           }
//         });
//     }
//   });
// });
// module.exports = fetch;

