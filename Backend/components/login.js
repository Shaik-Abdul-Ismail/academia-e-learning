

//----------------------------------------------------this is for mongoose Atlas online Database------------------------


const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const User = mongoose.model('User'); // Assuming you have a User model

router.get('/:email/:password', async (req, res) => {
  try {
    const user = await User.findOne({ email: req.params.email, password: req.params.password });
    if (user) {
      res.send({ message: '1 document inserted' });
    } else {
      res.send({ message: 'Record Not Found...' });
    }
  } catch (error) {
    console.error(error);
    res.status(500).send({ message: 'Internal Server Error' });
  }
});

module.exports = router;


//--------------------------------------------------------this is for local Database-------------------------------------------


// //sigin
// let mongodb = require("mongodb");
// let client = mongodb.MongoClient;
// let login = require("express")
//   .Router()
//   .get("/:email/:password", (req, res) => {
//     client.connect("mongodb://127.0.0.1:27017/new", (err, db) => {
//       if (err) {
//         throw err;
//       } else {
//         console.log("fetch called");
//         db.collection("data")
//           .find({ email: req.params.email, password: req.params.password })
//           .toArray((err, array) => {
//             if (err) {
//               throw err;
//             } else {
//               if (array.length > 0) {
//                 res.send({ message: "1 document inserted" });
//                 // res.send(array)
//               } else {
//                 res.send(array);
//               }
//               db.close();
//             }
//           });
//       }
//     });
//   });

// module.exports = login;




