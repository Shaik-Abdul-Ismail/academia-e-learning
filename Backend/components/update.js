

//----------------------------------------------------this is for mongoose Atlas online Database------------------------


const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const User = mongoose.model('User'); // Assuming you have a User model

router.put('/', async (req, res) => {
  try {
    const filter = { email: req.body.email };
    const update = {
      $set: {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        password: req.body.password,
      },
    };
    await User.updateOne(filter, update);
    res.send({ message: 'Document updated' });
  } catch (error) {
    console.error(error);
    res.status(500).send({ message: 'Internal Server Error' });
  }
});

module.exports = router;



//--------------------------------------------------------this is for local Database-------------------------------------------


// let mongodb = require("mongodb");
// let client = mongodb.MongoClient;
// let AdminUpdate = require("express").Router().put("/", (req, res) => {
//     client.connect("mongodb://127.0.0.1:27017/new", (err, db) => {
//         if (err) {
//             throw err;
//         } else {
//             var newvalaue = {
//                 $set: {
//                     firstName: req.body.firstName,
//                     lastName: req.body.lastName,
//                     email: req.body.email,
//                     password: req.body.password,
//                 }
//             };
//             db.collection("data").updateOne({ email: req.body.email }, newvalaue, (err, result) => {
//                 if (err) {
//                     throw err;
//                 } else {
//                     res.send({ message: "document updated" });
//                     db.close(); 
//                 }
//             });
//         }
//     });
// });
// module.exports = AdminUpdate;
