import React from 'react';
import { price } from '../../dummydata';
import { Link } from 'react-router-dom';

const PriceCard = () => {
  return (
    <>
      {price.map((val) => (
        <div className="items shadow" key={val.name}>
          <h4>{val.name}</h4>
          <h1>
            <span>&#x20B9;</span>
            {val.price}
          </h1>
          <p>{val.desc}</p>

          <div>
            <Link to="/payment">
              <button>GET STARTED</button>
            </Link>
          </div>
        </div>
      ))}
    </>
  );
};

export default PriceCard;

// import React, { useState } from 'react';
// import { price } from '../../dummydata';
// import PaymentModal from './payment';

// const PriceCard = () => {
//   const [showModal, setShowModal] = useState(false);

//   const openModal = () => {
//     setShowModal(true);
//   };

//   const closeModal = () => {
//     setShowModal(false);
//   };

//   return (
//     <>
//       {price.map((val) => (
//         <div className="items shadow" key={val.name}>
//           <h4>{val.name}</h4>
//           <h1>
//             <span>&#x20B9;</span>
//             {val.price}
//           </h1>
//           <p>{val.desc}</p>

//           <div>
//             <button onClick={openModal}>GET STARTED</button>
//             {console.log("clicked")}
//           </div>
//         </div>
//       ))}
//       <PaymentModal isOpen={showModal} closeModal={closeModal} />
//     </>
//   );
// };

// export default PriceCard;



// import React, { useState } from "react";
// import { price } from "../../dummydata";

// const PriceCard = () => {
//   const [showModal, setShowModal] = useState(false);
//   const openModal = () => {
//     setShowModal(true);
//   };

//   return (
//     <>
//       {price.map((val) => (
//         <div className="items shadow">
//           <h4>{val.name}</h4>
//           <h1>
//             <span>&#x20B9;</span>
//             {val.price}
//           </h1>
//           <p>{val.desc}</p>

//           <div>
//             <button onClick={openModal}>GET STARTED</button>
//           </div>
//         </div>
//       ))}
//     </>
//   );
// };

// export default PriceCard;
