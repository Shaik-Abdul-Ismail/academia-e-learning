import React, { useState } from "react";
import "./payment.css";

const PaymentForm = () => {
  const [cardNumber, setCardNumber] = useState("");
  const [expirationDate, setExpirationDate] = useState("");
  const [cvc, setCVC] = useState("");
  const [paymentAmount, setPaymentAmount] = useState("");
  const [paymentStatus, setPaymentStatus] = useState("");

  const handlePayment = () => {
    if (!cardNumber || !expirationDate || !cvc || !paymentAmount) {
      // Check if any of the fields are empty
      alert("All fields are required. Please fill in all the fields.");
      return; // Exit the function if any field is empty
    }

    setPaymentStatus("Processing Payment...");

    setTimeout(() => {
      setPaymentStatus("Payment Successful");
    }, 2000);
  };

  return (
    <div className="payment-form">
      <h2>Payment Form</h2>
      <div className="input-group">
        <label>Card Number:</label>
        <input
          type="text"
          value={cardNumber}
          onChange={(e) => setCardNumber(e.target.value)}
        />
      </div>
      <div className="input-group">
        <label>Expiration Date:</label>
        <input
          type="text"
          value={expirationDate}
          onChange={(e) => setExpirationDate(e.target.value)}
        />
      </div>
      <div className="input-group">
        <label>CVC:</label>
        <input
          type="text"
          value={cvc}
          onChange={(e) => setCVC(e.target.value)}
        />
      </div>
      <div className="input-group">
        <label>Amount:</label>
        <input
          type="number"
          value={paymentAmount}
          onChange={(e) => setPaymentAmount(e.target.value)}
        />
      </div>
      <div className="button-group">
        <button onClick={handlePayment}>Pay</button>
      </div>
      <div className={`payment-status ${paymentStatus && "fade-in"}`}>
        {paymentStatus}
      </div>
    </div>
  );
};

export default PaymentForm;



// import React, { useState } from "react";
// import "./payment.css";

// const PaymentForm = () => {
//   const [cardNumber, setCardNumber] = useState("");
//   const [expirationDate, setExpirationDate] = useState("");
//   const [cvc, setCVC] = useState("");
//   const [paymentAmount, setPaymentAmount] = useState("");
//   const [paymentStatus, setPaymentStatus] = useState("");
//   const [error, setError] = useState("");

//   const validateCardNumber = (input) => {
//     // Example card number pattern: 1111 2222 3333 4444
//     const cardNumberPattern = /^(\d{4}[\s-]?){4}\d{4}$/;
//     return cardNumberPattern.test(input);
//   };

//   const validateExpirationDate = (input) => {
//     // Example expiration date pattern: MM/YY
//     const expirationDatePattern = /^(0[1-9]|1[0-2])\/\d{2}$/;
//     return expirationDatePattern.test(input);
//   };

//   const validateCVC = (input) => {
//     // Example CVC pattern: 123 (3-digit CVC)
//     const cvcPattern = /^\d{3}$/;
//     return cvcPattern.test(input);
//   };

//   const handlePayment = () => {
//     if (!validateCardNumber(cardNumber)) {
//       setError("Invalid Card Number");
//     } else if (!validateExpirationDate(expirationDate)) {
//       setError("Invalid Expiration Date");
//     } else if (!validateCVC(cvc)) {
//       setError("Invalid CVC");
//     } else {
//       setPaymentStatus("Processing Payment...");
//       setError("");

//       setTimeout(() => {
//         setPaymentStatus("Payment Successful");
//       }, 2000);
//     }
//   };

//   return (
//     <div className="payment-form">
//       <h2>Payment Form</h2>
//       {error && <div className="error-message">{error}</div>}
//       <div className="input-group">
//         <label>Card Number:</label>
//         <input
//           type="text"
//           value={cardNumber}
//           required
//           onChange={(e) => setCardNumber(e.target.value)}
//         />
//       </div>
//       <div className="input-group">
//         <label>Expiration Date (MM/YY):</label>
//         <input
//           type="text"
//           value={expirationDate}
//           required
//           onChange={(e) => setExpirationDate(e.target.value)}
//         />
//       </div>
//       <div className="input-group">
//         <label>CVC:</label>
//         <input
//           type="text"
//           value={cvc}
//           required
//           onChange={(e) => setCVC(e.target.value)}
//         />
//       </div>
//       <div className="input-group">
//         <label>Amount:</label>
//         <input
//           type="number"
//           value={paymentAmount}
//           required
//           onChange={(e) => setPaymentAmount(e.target.value)}
//         />
//       </div>
//       <div className="button-group">
//         <button onClick={handlePayment}>Pay</button>
//       </div>
//       <div className={`payment-status ${paymentStatus && "fade-in"}`}>
//         {paymentStatus}
//       </div>
//     </div>
//   );
// };

// export default PaymentForm;




// import React, { useState } from "react";
// import "./payment.css";

// const PaymentForm = () => {
//   const [cardNumber, setCardNumber] = useState("");
//   const [expirationDate, setExpirationDate] = useState("");
//   const [cvc, setCVC] = useState("");
//   const [paymentAmount, setPaymentAmount] = useState("");
//   const [paymentStatus, setPaymentStatus] = useState("");

//   const handlePayment = () => {
    
//     setPaymentStatus("Processing Payment...");

//     setTimeout(() => {
//       setPaymentStatus("Payment Successful");
//     }, 2000);
//   };

//   return (
//     <div className="payment-form">
//       <h2>Payment Form</h2>
//       <div className="input-group">
//         <label>Card Number:</label>
//         <input
//           type="number"
//           value={cardNumber}
//           required

//           onChange={(e) => setCardNumber(e.target.value)}
//         />
//       </div>
//       <div className="input-group">
//         <label>Expiration Date:</label>
//         <input
//           type="number"
//           value={expirationDate}
//           required
//           onChange={(e) => setExpirationDate(e.target.value)}
//         />
//       </div>
//       <div className="input-group">
//         <label>CVC:</label>
//         <input
//           type="number"
//           value={cvc}
//           required
//           onChange={(e) => setCVC(e.target.value)}
//         />
//       </div>
//       <div className="input-group">
//         <label>Amount:</label>
//         <input
//           type="number"
//           value={paymentAmount}
//           required
//           onChange={(e) => setPaymentAmount(e.target.value)}
//         />
//       </div>
//       <div className="button-group">
//         <button onClick={handlePayment}>Pay</button>
//       </div>
//       <div className={`payment-status ${paymentStatus && "fade-in"}`}>
//         {paymentStatus}
//       </div>
//     </div>

   
//   );
// };

// export default PaymentForm;

// import React from 'react';
// import "./payment.css"
// const PaymentModal = ({ isOpen, closeModal }) => {
//   return (
//     <div className={`modal ${isOpen ? 'show' : 'hide'}`}>
//       <div className="modal-content">
//         <span className="close" onClick={closeModal}>&times;</span>
//         <h2>Payment Details</h2>
//         {/* Add your payment details content here */}
//         <p>Payment content goes here.</p>
//       </div>
//     </div>
//   );
// };

// export default PaymentModal;

// // import React, { useState } from 'react';
// // import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
// // import PriceCard from './PriceCard';

// // function Example() {
// //   const [modal, setModal] = useState(false);

// //   const toggle = () => setModal(!modal);

// //   return (
// //     <div>

// //       <Modal isOpen={modal} toggle={toggle}>
// //         <ModalHeader toggle={toggle}>Modal title</ModalHeader>
// //         <ModalBody>
// //           king
// //         </ModalBody>
// //         <ModalFooter>
// //           <Button color="primary" onClick={toggle}>
// //             Do Something
// //           </Button>{' '}
// //           <Button color="secondary" onClick={toggle}>
// //             Cancel
// //           </Button>
// //         </ModalFooter>
// //       </Modal>
// //     </div>
// //   );
// // }

// // export default Example;
