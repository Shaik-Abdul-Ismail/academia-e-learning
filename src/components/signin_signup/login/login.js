import React, { useState } from "react";
import "./login.css";
import Img from "./img.png";
import axios from "axios";
// import Navbar from "../../navbar/navbar";
import { Link, useNavigate } from "react-router-dom";
// import Admin from "../Admin/admin";

const LoginForm = () => {
  const [email, setemail] = useState("");
  const [password, setpassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [emailError, setEmailError] = useState("");
  const [passwordError, setPasswordError] = useState("");
  // const [isLoginIn, setisLoginIn] = useState(false);
  const navigate = useNavigate();

  const adminCredentials = {
    email: "ismail@gmail.com",
    password: "ismail@88",
  };

  const handleShowPasswordChange = () => {
    setShowPassword((prevShowPassword) => !prevShowPassword);
  };

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    if (name === "email") {
      setemail(value);
      setEmailError("");
    } else if (name === "password") {
      setpassword(value);
      setPasswordError("");
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    let valid = true;

    setEmailError("");
    setPasswordError("");

    if (!email.trim()) {
      setEmailError("Email is required");
      valid = false;
    }

    if (!password.trim()) {
      setPasswordError("Password is required");
      valid = false;
    }

    if (valid) {
      
    }
  };
  const handleLogin = async (event) => {
    event.preventDefault();
    setemail("");
    setpassword("");

    try {
      const response = await axios.get(
        `http://localhost:4000/login/${email}/${password}`,                  //      https://academia-e-learning-frontend.onrender.com/
        {
          email,
          password,
        }
      );
      if (
        email === adminCredentials.email &&
        password === adminCredentials.password
      ) {
        // console.log("admin");
        navigate("/admin");
        // setisLoginIn(true);
      } else if (response.data.message === "1 document inserted") {
        // alert("login success");
        console.log("user")
        navigate("/home");
        // setisLoginIn(true);/
      } else {
        alert("login failed");
        navigate("/register")
      }
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div className="container_signin" style={{ background: "black" }}>
      <img className="learn" src={Img} alt="learning" />
      <div className="forms-container">
        <div className="signin">
          <form className="sign-in-form" onSubmit={handleSubmit}>
            <h2 className="title">Login</h2>
            <div className="input-fields">
              <i className="fa fa-user " style={{"padding-top":"20px"}}></i>
              <input
                type="text"
                placeholder="Email"
                name="email"
                value={email}
                required
                onChange={handleInputChange}
              />
              <p className="error">{emailError}</p>
            </div>
            <div className="input-fields">
              <i className="fa fa-lock " style={{"padding-top":"20px"}}></i>
              <input
                type={showPassword ? "text" : "password"}
                placeholder="Password"
                name="password"
                value={password}
                required
                onChange={handleInputChange}
              />
              <p className="error">{passwordError}</p>
            </div>
            <input
              type="submit"
              value="Login"
              className="btn solid"
              onClick={handleLogin}
            />
            <Link to="/register"><button value="login" className="btn solid" >Register</button> </Link>
            {/* {isLoginIn ? (email === adminCredentials.email && password ===
            adminCredentials.password ? "true" : "flase") : ""} */}

          </form>
        </div>
      </div>
    </div>
  );
};

export default LoginForm;
