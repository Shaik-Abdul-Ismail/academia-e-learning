//signup
import React, { useState } from 'react';
import './register.css';
import axios from "axios"
import Tag from './register_icon.png';
import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';

const RegistrationForm = () => {
  const navigate = useNavigate()
  const [firstName, setfirstName] = useState('');
  const [lastName, setlastName] = useState('');
  const [email, setemail] = useState('');
  const [password, setpassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [emailError, setEmailError] = useState('');
  const [passwordError, setPasswordError] = useState('');
  
  const handleRegister = async (event) => {
    event.preventDefault();
    setfirstName("");
    setlastName("");
    setemail("");
    setpassword("");
    

    try {
      const response = await axios.post("http://localhost:4000/register ", {          //   //https://academia-e-learning-frontend.onrender.com
        firstName,
        lastName,
        email,
        password,
      });

      if (response.data.message === "data inserted") {
        alert("Registration failed");
      } else {
        alert("Registration success");
        navigate('/login')
      }
    } catch (error) {
      console.error(error);
    }
    };

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    if (name === 'firstName') {
      setfirstName(value);
    } else if (name === 'lastName') {
      setlastName(value);
    } else if (name === 'email') {
      setemail(value);
      setEmailError('');
    } else if (name === 'password') {
      setpassword(value);
    } else if (name === 'confirmPassword') {
      setConfirmPassword(value);
      setPasswordError('');
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    let valid = true;

    // Reset previous error messages
    setEmailError('');
    setPasswordError('');

    // Email validation
    if (!email.trim()) {
      setEmailError('Email is required');
      valid = false;
    }

    // Password validation
    if (password !== confirmPassword) {
      setPasswordError('Passwords do not match');
      valid = false;
    }

    if (valid) {
      // Perform form submission or other actions here
    }
  };

  return (
    <div className="main-container">
      <div className="forms-container">
        <div className="signin-signup">
          <img className="iconic" src={Tag} alt="Registration Icon" />
          <form className="registration-form" onSubmit={handleSubmit}>
            <h2 className="title">Register</h2>
            <div className="input-field">
              <input
                type="text"
                placeholder="First Name"
                name="firstName"
                value={firstName}
                required
                onChange={handleInputChange}
              />
            </div>
            <div className="input-field">
              <input
                type="text"
                placeholder="Last Name"
                name="lastName"
                value={lastName}
                required
                onChange={handleInputChange}
              />
            </div>
            <div className="input-field">
              <input
                type="text"
                placeholder="Email"
                name="email"
                value={email}
                required
                onChange={handleInputChange}
              />
              <p className="error">{emailError}</p>
            </div>
            <div className="input-field">
              <input
                type="password"
                placeholder="Password"
                name="password"
                value={password}
                required
                onChange={handleInputChange}
              />
            </div>
            <div className="input-field">
              <input
                type="password"
                placeholder="Confirm Password"
                name="confirmPassword"
                value={confirmPassword}
                required
                onChange={handleInputChange}
              />
              <p className="error">{passwordError}</p>
            </div>
            <input type="submit" value="Register" className="btn solid" onClick={handleRegister}/>
            <Link to="/login">
            <button value="login" className="btn solid"> Login </button>
            </Link>
          </form>
        </div>
      </div>
    </div>
  );
};

export default RegistrationForm;
