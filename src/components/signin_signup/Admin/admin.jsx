import React, { useState, useEffect } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import "./Admin.css";

function Admin() {
  const [data, setData] = useState([]);
  const [updateData, setUpdateData] = useState({
    firstName: "",
    lastName: "",
    email: "",
    password: "",
  });

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await axios.get("http://localhost:4000/fetch");
      // const response = await axios.get("https://academia-e-learning.onrender.com");
      // const combinedData = [...responseLocal.data, ...responseExternal.data];  // for running the both and change  the response into this names
      setData(response.data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const handleDelete = async (email) => {
    try {
      const confirmed = window.confirm("Are you sure you want to delete this data?");

      if (confirmed) {
        await axios.delete(`http://localhost:4000/delete/${email}`);
        // await axios.delete(`https://academia-e-learning.onrender.com/delete/${email}`);
        fetchData();
      }
    } catch (error) {
      console.error("Error deleting data:", error);
    }
  };

  const handleUpdate = async () => {
    try {
      const updatedData = {
        firstName: updateData.firstName,
        lastName: updateData.lastName,
        email: updateData.email,
        password: updateData.password,
      };

      await axios.put("http://localhost:4000/update", updatedData);
      // await axios.put("https://academia-e-learning.onrender.com/update", updatedData);
      console.log("Data updated:", updatedData);
      fetchData();
      setUpdateData({
        firstName: "",
        lastName: "",
        email: "",
        password: "",
      });
    } catch (error) {
      console.error("Error updating data:", error);
    }
  };

  return (
    <div className="admin-container">
      <h2>Admin Panel</h2>
      <button>
        <Link to="/home">Home</Link>
      </button>
      <button onClick={fetchData}>Refresh Data</button>
      <table>
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Password</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {data.map((item) => (
            <tr key={item._id}>
              <td>{item.firstName}</td>
              <td>{item.lastName}</td>
              <td>{item.email}</td>
              <td>{item.password}</td>
              <td>
                <button onClick={() => handleDelete(item.email)}>Delete</button>
                <button
                  onClick={() =>
                    setUpdateData({
                      firstName: item.firstName,
                      lastName: item.lastName,
                      email: item.email,
                      password: item.password,
                    })
                  }
                >
                  Update
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>

      <div className="update-form">
        <h3>Update Data</h3>
        <input
          type="text"
          placeholder="Email"
          value={updateData.email}
          onChange={(e) =>
            setUpdateData({ ...updateData, email: e.target.value })
          }
        />
        <input
          type="text"
          placeholder="New First Name"
          value={updateData.firstName}
          onChange={(e) =>
            setUpdateData({ ...updateData, firstName: e.target.value })
          }
        />
        <input
          type="text"
          placeholder="New Last Name"
          value={updateData.lastName}
          onChange={(e) =>
            setUpdateData({ ...updateData, lastName: e.target.value })
          }
        />
        <input
          type="text"
          placeholder="New Password"
          value={updateData.password}
          onChange={(e) =>
            setUpdateData({ ...updateData, password: e.target.value })
          }
        />
        <button onClick={handleUpdate}>Update</button>
      </div>
    </div>
  );
}

export default Admin;
