import React from 'react';
import { Link } from 'react-router-dom';

import './style.css' 
function Home() {
  return (
    <div className="body1">
      <h2 className='home-heading'>WELCOME TO ACADEMIA
ONLINE EDUCATION & LEARNING</h2>
      <h2 className='home-heading1'>REGISTER TO GET TO KNOW MORE ABOUT US</h2>

      <p className="para home-heading">Already a member? Login In by hovering below</p>

      <Link to="/register">
        <button className="homeButton1">Register</button>
      </Link>

      <Link to="/login">
        <button className="homeButton1">Login</button>
      </Link>
    </div>
  );
}

export default Home;

