import React from "react";
import "./about.css";
import Back from "../common/back/Back";
import Header from "../common/header/Header";
import AboutCard from "./AboutCard";

const About = () => {
  return (
    <>
      <Header />
      <Back title="About Us" />
      <AboutCard />
    </>
  );
};

export default About;
