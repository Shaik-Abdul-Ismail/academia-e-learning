
import "./App.css";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Mainpage from "./components/signin_signup/home/home";
import Login from "./components/signin_signup/login/login";
import Register from "./components/signin_signup/register/register";
import Home from "./components/home/Home";
import About from "./components/about/About";
import CourseHome from "./components/allcourses/CourseHome";
import Team from "./components/team/Team";
import Pricing from "./components/pricing/Pricing";
import Blog from "./components/blog/Blog";
import Contact from "./components/contact/Contact";
import  Payment  from "./components/pricing/payment";
import Admin from "./components/signin_signup/Admin/admin";

function App() {
  return (
    <>
      <Router>
        <Routes>
          <Route path="/" element={<Mainpage />} />
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
        </Routes>
        <div>
          <Routes>
            <Route path="/admin" element={<Admin />}></Route>
            <Route path="/payment" element={<Payment />} />
          </Routes>
        </div>
        <div>
          
          <Routes>
            <Route path="/home" element={<Home />} />
            <Route path="/about" element={<About />} />
            <Route path="/courses" element={<CourseHome />} />
            <Route path="/team" element={<Team />} />
            <Route path="/pricing" element={<Pricing />} />
            <Route path="/journal" element={<Blog />} />
            <Route path="/contact" element={<Contact />} />
          </Routes>
          
        </div>
        
      </Router>
    </>
  );
}

export default App;
